import './App.css';
import Girl1 from './assets/images/girl1.PNG';
import Girl2 from './assets/images/girl2.PNG';
import BackImage from './assets/images/back.PNG';
function App() {
  return (
    <div className="App">
        <div id="label">
          Free Assembly Spring 2022
        </div>
        <div id="girl1" style={{
          backgroundImage: `url(${Girl1})`
        }} />
        <div id="girl2" style={{
          backgroundImage: `url(${Girl2})`
        }} />
        <div id="back-image" style={{
          backgroundImage: `url(${BackImage})`
        }} >
          <div id="label">Jackets</div>
        </div>
    </div>
  );
}

export default App;
